package com.example.estudiante.menuapp

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Idservicio.setOnClickListener {
            idView.setText("Boton Seleccionando :Servicio")
            selectedButon(it)
        }
        Idportafolio.setOnClickListener {

            idView.setText("Boton Seleccionando :Portafolio ")
            val intent: Intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }
        Idacerca.setOnClickListener {
        idView.setText("Acervad de Nosotros"
        )}
        IDContacto.setOnClickListener {
        idView.setText("contactos")
        }
        IdRedSocial.setOnClickListener {
        idView.setText("Boton de Redes Sociales")
        }

    }


    fun selectedButon(view: View){

        if ( view.id=== R.id.Idservicio) {
            Toast.makeText( view.context, "Boton de Servicios redirecionando", Toast.LENGTH_LONG).show()
            val intent: Intent = Intent(view.context, SecondActivity::class.java)
            startActivity(intent)

        }
    }

}
